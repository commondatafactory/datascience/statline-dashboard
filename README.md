
<!-- README.md is generated from README.Rmd. Please edit that file -->

# statline

<!-- badges: start -->
<!-- badges: end -->

The goal of statline is to provide a dashboard to easily search the
statline database.

## Disclaimer

Dit is experimentele code in de testfase waar nog aan gewerkt wordt!

## Installation

Het pakket ‘statline’ kan geïnstalleerd worden vanuit GitLab als:

``` r
# Install the development version from GitLab
devtools::install_gitlab("commondatafactory/datascience/statline-dashboard")
```

Na installatie van het pakket kan het dashboard worden gedraaid door
`run_app()` uit te voeren, zie voorbeeld hieronder.

## Example

You can start the dashboard by:

``` r
library(statline)

# Launch the dashboard
run_app()
```

## Licentie

<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl"><img alt="Creative Commons-Licentie" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dit
werk valt onder een
<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl">Creative
Commons Naamsvermelding-NietCommercieel-GelijkDelen 4.0
Internationaal-licentie</a>.
